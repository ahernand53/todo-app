import { Pipe, PipeTransform } from '@angular/core';
import { Todo } from '../models/todo.model';

@Pipe({
  name: 'completedFilter',
  pure: false
})
export class CompletedFilterPipe implements PipeTransform {

  transform(todos: Todo[], completed: boolean ): Todo[] {

    return todos.filter( todo => todo.completed === completed );

  }

}
