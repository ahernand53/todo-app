import { Injectable } from '@angular/core';
import { Todo } from '../models/todo.model';

@Injectable({
  providedIn: 'root'
})
export class TodoService {

  todos: Todo[] = [];

  constructor() {

    this.loadStorage();

   }

   createTodo( title:string ) {
    const newTodo = new Todo(title);
    this.todos.push(newTodo);
    this.saveStorage();

    return newTodo.id;
   }

   getTodo( id: string | number) {

    id = Number(id);
    return this.todos.find( todoData => todoData.id === id);

   }

   deleteTodos( todo: Todo ) {

    this.todos = this.todos.filter( todoData => todoData.id !== todo.id);
    
    this.saveStorage();

   }

   saveStorage() {
    
    localStorage.setItem('data', JSON.stringify(this.todos) );

   }

   loadStorage() {
     if (localStorage.getItem('data')) {
      this.todos = JSON.parse(localStorage.getItem('data'));
     }
   }
}
