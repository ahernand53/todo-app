import { TodoItem } from './todo-item.model';


export class Todo {

    id: number;
    title: string;
    createdAt: Date;
    completedAt: Date;
    completed: boolean;
    items: TodoItem[];

    constructor( title: string ) {

        this.title = title;
        this.createdAt = new Date();
        this.completed = false;
        this.items = [];

        this.id = new Date().getTime();

    }

}