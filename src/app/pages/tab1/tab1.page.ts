import { Component } from '@angular/core';
import { TodoService } from '../../services/todo.service';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {

  constructor( private router: Router,
               private todoService: TodoService,
               private alertCtrl: AlertController ) {
  }


  async addTodo() {

    const alert = await this.alertCtrl.create({
      header: 'Nueva lista',
      inputs: [
        {
          name: 'title',
          type: 'text',
          placeholder: 'Nombre de la lista'
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Crear',
          handler: (data) => {
            console.log(data);
            if( data.title.length === 0) {
              return;
            }

            // Tengo que crear la lista
            const todoId = this.todoService.createTodo( data.title );
            this.router.navigateByUrl(`/tabs/tab1/add/${todoId}`);
          }
        }
      ]
    })
    alert.present();

  }

}


