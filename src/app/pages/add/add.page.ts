import { Component, OnInit } from '@angular/core';
import { TodoService } from '../../services/todo.service';
import { ActivatedRoute } from '@angular/router';
import { Todo } from '../../models/todo.model';
import { TodoItem } from '../../models/todo-item.model';

@Component({
  selector: 'app-add',
  templateUrl: './add.page.html',
  styleUrls: ['./add.page.scss'],
})
export class AddPage implements OnInit {

  todo: Todo;
  nameItem = '';

  constructor( private todoService: TodoService,
               private router: ActivatedRoute) {

    const todoId = this.router.snapshot.paramMap.get('todoId');

    this.todo = this.todoService.getTodo(todoId);

  }

  ngOnInit() {
  }

  addItem() {

    if( this.nameItem.length === 0 ) {
      return;
    }

    const newItem = new TodoItem( this.nameItem );
    this.todo.items.push( newItem );

    this.nameItem = '';
    this.todoService.saveStorage();

  }

  changeCheck( item: TodoItem) {
    
    const pending = this.todo.items.filter( itemData => !itemData.completed)
    .length;

    if (pending === 0 ) {
      this.todo.completedAt = new Date();
      this.todo.completed = true;
    } else {
      this.todo.completedAt = null;
      this.todo.completed = false;
    }

    this.todoService.saveStorage();
  }

  deleteItem( i: number) {

    this.todo.items.splice( i, 1 );

    this.todoService.saveStorage();

  }

}
