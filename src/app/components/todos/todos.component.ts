import { Component, OnInit, Input } from '@angular/core';
import { Todo } from '../../models/todo.model';
import { Router } from '@angular/router';
import { TodoService } from '../../services/todo.service';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.scss'],
})
export class TodosComponent implements OnInit {

  @Input() completed = true;

  constructor( private router: Router,
               public todoService: TodoService,
               private alertCtrl: AlertController) { }

  ngOnInit() {}

  todoSelected( todo: Todo ) {

    if (this.completed) {
      this.router.navigateByUrl(`/tabs/tab2/add/${todo.id}`);
    } else {
      this.router.navigateByUrl(`/tabs/tab1/add/${todo.id}`);
    }


  }

  deleteTodos( todo: Todo ) {

    this.todoService.deleteTodos( todo );

  }

  async editTodo( todo: Todo) {

    const alert = await this.alertCtrl.create({
      header: 'Editar Lista',
      inputs: [
        {
          name: 'title',
          type: 'text',
          value: todo.title,
          placeholder: 'Nombre de la lista',
        }
      ],
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancelar');
          }
        },
        {
          text: 'Editar',
          handler: (data) => {
            console.log(data);
            if( data.title.length === 0) {
              return;
            }
            todo.title = data.title;
            this.todoService.saveStorage();
            return;
          }
        }
      ]
    })
    alert.present();


  }

}
